local executor_prefix = "<leader>k"
local tab = "<Tab>"
local nt_prefix = "<leader>N"
local nt = require "neotest"
local win_prefix = "<leader>W"
return {
  -- first key is the mode
  n = {
    [tab] = { desc = "Tab" },
    [tab .. "n"] = { "<cmd>tabnew<cr>", desc = "Tab new" },
    [tab .. "d"] = { "<cmd>tabclose<cr>", desc = "Tab close" },
    ["]" .. tab] = { "<cmd>tabnext<cr>", desc = "Next tab" },
    ["[" .. tab] = { "<cmd>tabprevious<cr>", desc = "Previous tab" },
    [executor_prefix] = { desc = "Executor" },
    [executor_prefix .. "r"] = { "<cmd>ExecutorRun<cr>", desc = "Executor Run" },
    [executor_prefix .. "s"] = { "<cmd>ExecutorSetCommand<cr>", desc = "Executor Set Command" },
    [executor_prefix .. "d"] = { "<cmd>ExecutorToggleDetail<cr>", desc = "Executor Toggle Detail" },
    [executor_prefix .. "S"] = { "<cmd>ExecutorSwapToSplit<cr>", desc = "Executor Swap to Split" },
    [executor_prefix .. "p"] = { "<cmd>ExecutorSwapToPopup<cr>", desc = "Executor Swap to Popup" },
    [executor_prefix .. "R"] = { "<cmd>ExecutorReset<cr>", desc = "Executor Reset" },
    [executor_prefix .. "h"] = { "<cmd>ExecutorShowHistory<cr>", desc = "Executor Show History" },
    ["<leader>dl"] = {
      require("dap.ext.vscode").load_launchjs,
      desc = "Load launch.json",
    },
    [nt_prefix .. "r"] = { function() nt.run.run() end, desc = "Run nearest test" },
    [nt_prefix .. "R"] = { function() nt.run.run { strategy = "dap" } end, desc = "Debug nearest test" },
    [nt_prefix .. "f"] = { function() nt.run.run(vim.fn.expand "%") end, desc = "Run file" },
    [nt_prefix .. "F"] = { function() nt.run.run { vim.fn.expand "%", strategy = "dap" } end, desc = "Debug file" },
    [nt_prefix .. "s"] = { function() nt.summary.toggle() end, desc = "Toggle summary" },
    [nt_prefix .. "o"] = { function() nt.output.open { enter = true } end, desc = "Open output" },
    [nt_prefix .. "p"] = { function() nt.output_panel.toggle() end, desc = "Toggle output panel" },
    [win_prefix .. "m"] = { "<cmd>WindowsMaximize<cr>", desc = "Maximize window" },
    [win_prefix .. "v"] = { "<cmd>WindowsMaximizeVertically<cr>", desc = "Maximize vertically" },
    [win_prefix .. "h"] = { "<cmd>WindowsMaximizeHorizontally<cr>", desc = "Maximize horizontally" },
    [win_prefix .. "e"] = { "<cmd>WindowsEqualize<cr>", desc = "Equalize window" },
    [win_prefix .. "a"] = { "<cmd>WindowsToggleAutowidth<cr>", desc = "Toggle autowidth" },
    ["<leader>bf"] = { "<cmd>cd %:h <CR>", desc = "Set Files Location As Dir" },
    ["<leader>uz"] = { require("notify").dismiss, desc = "Dismiss All Notifications" },
  },
}
