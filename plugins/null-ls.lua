local h = require "null-ls.helpers"
local methods = require "null-ls.methods"

local FORMATTING = methods.internal.FORMATTING

local typstfmt = {
  name = "typstfmt",
  method = FORMATTING,
  filetypes = { "typst" },
  generator = h.formatter_factory {
    command = "typstfmt",
    args = { "--output", "-" },
    to_stdin = true,
  },
}

return {
  "jose-elias-alvarez/null-ls.nvim",
  opts = function(_, config)
    local null_ls = require "null-ls"
    config.sources = {
      null_ls.builtins.formatting.fish_indent,
      null_ls.builtins.formatting.prettierd,
      null_ls.builtins.formatting.clang_format.with {
        filetypes = { "cuda", "proto" },
      },
      null_ls.builtins.diagnostics.ruff.with {
        disabled_filetypes = { "python" },
      },
      -- typstfmt,
    }
    return config -- return final config table
  end,
}
