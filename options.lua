return {
  opt = {
    relativenumber = true,
    number = true,
    spell = false,
    signcolumn = "auto",
    wrap = false,
    spellfile = "/home/davidk/.config/nvim/lua/user/spell/en.utf-8.add",
    background = (function()
      local cs = (vim.fn.system "distrobox-host-exec gsettings get org.gnome.desktop.interface color-scheme")
        :gsub("\n", "")
        :gsub("'", "")
      if cs == "prefer-dark" then
        return "dark"
      else
        return "light"
      end
    end)(),
  },
  g = {
    mapleader = " ",
    autoformat_enabled = true,
    cmp_enabled = true,
    autopairs_enabled = true,
    diagnostics_mode = 3,
    icons_enabled = true,
    ui_notifications_enabled = true,
  },
}
