return {
  settings = {
    ltex = {
      checkFrequency = "save",
      dictionary = (function()
        local dict = {}
        local path = "/home/davidk/.config/nvim/lua/user/spell/en.utf-8.add"
        local words = {}
        for word in io.open(path, "r"):lines() do
          table.insert(words, word)
        end
        dict["en-US"] = words
        dict["en-GB"] = words
        return dict
      end)(),
    },
  },
}
